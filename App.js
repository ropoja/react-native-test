import React, {Component} from 'react';
import {createStackNavigator} from 'react-navigation';
import PantallaDetalles from './src/pantallas/Detalles';
import PantallaInicio from './src/pantallas/Inicio';
import FlexDirectionBasics from './src/pantallas/FlexDirectionBasics';
import JustifyContentBasics from "./src/pantallas/JustifyContentBasics";
import AlignItemsBasics from "./src/pantallas/AlignItemsBasics";
import BottomTabNavigator from "./src/pantallas/BottomTabNavigator";

const RootStack = createStackNavigator(
    {
        Inicio: PantallaInicio,
        Detalles: PantallaDetalles,
        FDB: FlexDirectionBasics,
        JCB: JustifyContentBasics,
        AIB: AlignItemsBasics,
        BTN: BottomTabNavigator
    },
    {
        initialRouteName: "Inicio",
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#f4511e',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        },
    }
);


export default class App extends Component {
    render() {
        return <RootStack/>;
    }
}