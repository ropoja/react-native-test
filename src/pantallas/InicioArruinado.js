import React, {Component} from 'react';
import {View, Text, Button, StyleSheet, Alert, TouchableOpacity} from 'react-native';
import Logo from '../componentes/Logo';

export default class PantallaInicio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contador: 0,
        };
        this.timer = null;
        this.addOne = this.addOne.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
    }

    static navigationOptions = ({navigation}) => {
        return {
            //title: "Inicio",
            headerTitle: <Logo texto="Inicio"/>,
            headerRight: (
                /*<TouchableOpacity onPressIn={this.addOne} onPressOut={this.stopTimer}>
                    <Text>+1</Text>
                </TouchableOpacity>*/
                <Button
                    //onPress={() => alert("¡Esto es un botón!")}
                    //onPress={navigation.getParam("incrementarContador")}
                    onPress={() => navigation.getParam("incrementarContador")()}
                    title="+1"
                    color="#000"
                />
            ),

            headerRightContainerStyle: {padding: 15}
        }
    };

    //region Metodos pre-DOM
    componentDidMount() {
        this.props.navigation.setParams({incrementarContador: this._incrementarContador});
    }

    //endregion

    _incrementarContador = () => {
        this.setState({contador: this.state.contador + 1});
    }

    _onPressButton() {
        Alert.alert('You tapped the button!')
    }

    //region Metodos hold button
    addOne() {
        this.setState({number: this.state.number + 1});
        this.timer = setTimeout(this.addOne, 200);
        this.addOne = this.addOne.bind(this);
        this.stopTimer = this.stopTimer.bind(this);
    }

    stopTimer() {
        clearTimeout(this.timer);
    }

    //endregion

    render() {
        return (
            <View style={{
                flex: 1,
                flexDirection: "column", justifyContent: "center",
                alignItems: "center",
            }}>
                <View style={{
                    flex: 1,
                    flexDirection: "column", justifyContent: "space-around",
                    alignItems: "center"
                }}>
                    <Text>Inicio</Text>
                    <Text>{this.state.contador}</Text>
                </View>
                /*<View style={{ alignItems: "center" }}>
                    <Button
                        title="Ir a Detalles"
                        //containerViewStyle={{width: "100%", marginLeft: 0, borderRadius: 20}}
                        onPress={() => {
                            this.props.navigation.navigate("Detalles", {
                                itemId: 86,
                                otroParametro: "lo que sea",
                            });
                        }}
                    />
                </View>*/
                <View style={{
                    flex: 2,
                    flexDirection: "row", justifyContent: "space-around",
                    alignItems: "center"
                }}>
                    <Button
                        title="Ir a Detalles"
                        //containerViewStyle={{width: "100%", marginLeft: 0, borderRadius: 20}}
                        onPress={() => {
                            this.props.navigation.navigate("Detalles", {
                                itemId: 86,
                                otroParametro: "lo que sea",
                            });
                        }}
                    />
                    <Button
                        onPress={this._onPressButton}
                        title="Press Me"
                    />
                </View>

                /* <View style={styles.buttonContainer}>
                    <Button
                        onPress={this._onPressButton}
                        title="Press Me"
                    />
                </View>

                <View style={styles.buttonContainer}>
                    <Button
                        onPress={this._onPressButton}
                        title="Press Me"
                        color="#841584"
                    />
                </View>

                <View style={styles.alternativeLayoutButtonContainer}>
                    <Button
                        onPress={this._onPressButton}
                        title="This looks great!"
                    />
                    <Button
                        onPress={this._onPressButton}
                        title="OK!"
                        color="#841584"
                    />
                </View>

                <Button
                    title="Otro Botón"
                    onPress={() => {
                        this.props.navigation.navigate("Detalles", {
                            itemId: 86,
                            otroParametro: "lo que sea",
                        });
                    }}
                />*/
            </View>
        )
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    button: {
        alignItems: "center",
        backgroundColor: "#DDDDDD",
        padding: 10
    },
    buttonContainer: {
        margin: 20
    },
    alternativeLayoutButtonContainer: {
        margin: 20,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    flex2: {
        flex: 2
    },
    marcoNegro: {
        borderColor: "black",
        borderWidth: 10
    }
})