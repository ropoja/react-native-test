import React, { Component } from 'react';
import { View } from 'react-native';

export default class AlignItemsBasics extends Component {
    render() {
        return (
            // Try setting `alignItems` to 'flex-start'
            // Try setting `justifyContent` to `flex-end`.
            // Try setting `flexDirection` to `row`.
            <View style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'stretch',
                padding: "2%"
            }}>
                <View style={{width: 50, backgroundColor: 'powderblue'}} />
                <View style={{width: 50, backgroundColor: 'skyblue'}} />
                <View style={{width: 100, backgroundColor: 'steelblue'}} />
                <View style={{width: 150, backgroundColor: 'green'}} />
            </View>
        );
    }
};