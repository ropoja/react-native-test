import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';

export default class PantallaDetalles extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    const { params } = navigation.state;

    return {
      title: params.otroParametro ? params.otroParametro : "Detalles",
      headerStyle: {
        backgroundColor: navigationOptions.headerTintColor
      },
      headerTintColor: navigationOptions.headerStyle.backgroundColor,
    };
  };

  render() {
    const { navigation } = this.props;
    const itemId = navigation.getParam("itemId", "NO-ID");
    var otroParametro = navigation.getParam("otroParametro", "default");

    return (
      <View style={{ flex:1, justifyContent: "space-evenly", alignItems: "center"}}>
        <Text>itemId: {JSON.stringify(itemId)} </Text>
        <Text>otroParametro: {JSON.stringify(otroParametro)} </Text>
        <Text>{JSON.stringify(this.props.navigation.state.params)} </Text>
        <Button       
          title = "Ir a los detalles de nuevo..."
          onPress={() => this.props.navigation.push("Detalles", {
            itemId: Math.floor(Math.random() * 100)
          })}
        />
        
        <Button 
          title="Ir a Inicio" 
          onPress={() => this.props.navigation.popToTop()}
        />

        <Button
          title="Atrás"
          onPress={() => this.props.navigation.goBack()}
        />
        
        <Button
          title="Actualizar título"
          onPress={() => this.props.navigation.setParams({otroParametro: "¡Actualizado!"})}
        />
      </View>
    );
  }
}
