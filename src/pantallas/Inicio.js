import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import Logo from '../componentes/Logo';

export default class PantallaInicio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contador: 0,
        };
    }

    static navigationOptions = ({navigation}) => {
        return {
            //title: "Inicio",
            headerTitle: <Logo titulo="Inicio"/>,
            headerRight: (
                <Button
                    //onPress={() => alert("¡Esto es un botón!")}
                    onPress={() => navigation.getParam("incrementarContador")()}
                    title="+1"
                    color="#000"

                />
            ),
            headerRightContainerStyle: {
                padding: 15
            },
        }
    };

    componentDidMount() {
        this.props.navigation.setParams({incrementarContador: this._incrementarContador});
    }

    _incrementarContador = () => {
        this.setState({contador: this.state.contador + 1});
    }

    renderTest = () => {
        return (
            <View style={{flex: 1, alignItems: "center", justifyContent: "space-around"}}>
                <Text>Inicio</Text>
                <Button
                    title="Ir a Detalles"
                    onPress={() => {
                        this.props.navigation.navigate("Detalles", {
                            itemId: 86,
                            otroParametro: "Título parametrizado",
                        });
                    }}
                />
                <Text>Contador: {this.state.contador}</Text>
            </View>
        )
    };

    render() {
        return (
            <View style={{flex: 1}}>
                {this.renderTest()}
            </View>
        )
    };
}