import React, {Component} from 'react';
import {Image, View, Text} from 'react-native';

export default class Logo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            texto: "Titulo"
        };
    }

    render() {
        return (
            <View style={{
                flex: 1,
                paddingLeft: "1%",
                justifyContent: "flex-start",
                flexDirection: "row",
                alignItems: "center",
            }}>
                <Image
                    source={require("../../img/splat.jpg")}
                    style={{width: 50, height: 50}}
                />
                <Text style={{paddingLeft: 15, fontSize: 30, }}>{this.props.texto}</Text>
            </View>
        );
    }
}
